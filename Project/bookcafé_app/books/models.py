from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=300)
    description = models.TextField()
    author = models.CharField(max_length=200)
    genre = models.CharField(max_length=100)
    ISBN = models.CharField(max_length=50)
    status = models.CharField(max_length=100)
    edition = models.CharField(max_length=50)

    def __str__(self):
        return self.title