from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
from .models import Book
from django.template import loader

def book_list(request):
    books = Book.objects.all()
    return render(request, 'book_list.html', {'books': books})



